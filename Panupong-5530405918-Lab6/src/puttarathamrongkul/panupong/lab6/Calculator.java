package puttarathamrongkul.panupong.lab6;

/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab6
 * 1. To learn how to create a window
 * 2. To learn how to use layout managers
 * 3. To learn how to create components
 * 4. To learn how to place components in the content pane
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class Calculator {
	//to create content panel in method
	public static void addComponents(Container contentPane) {
		contentPane.setLayout(new FlowLayout());
		JTextField showTextField = new JTextField(30);
		contentPane.add(showTextField);
		
	}
	
	//to create createAndShowGUI method
	public static void createAndShowGUI() {
		//to create new frame and panel
		JFrame frame = new JFrame("Calculator");
		JPanel content = new JPanel();
		JPanel buttonPane = new JPanel();
		addComponents(content);
		buttonPane.setLayout(new GridLayout(4, 4));
		//to create new button and set the button
		JPanel buttonP1 = new JPanel();
		JButton n1Button = new JButton("1");
		buttonP1.add(n1Button);
		buttonP1.setSize(90, 30);
		n1Button.setSize(90, 30);
		buttonP1.setBackground(Color.gray);
		
		
		JPanel buttonP2 = new JPanel();
		JButton n2Button = new JButton("2");
		buttonP2.add(n2Button);
		buttonP2.setSize(90, 30);
		n2Button.setSize(90, 30);
		buttonP2.setBackground(Color.gray);
		
		JPanel buttonP3 = new JPanel();
		JButton n3Button = new JButton("3");
		buttonP3.add(n3Button);
		buttonP3.setSize(90, 30);
		n3Button.setSize(90, 30);
		buttonP3.setBackground(Color.gray);
		
		JPanel buttonP4 = new JPanel();
		JButton n4Button = new JButton("4");
		buttonP4.add(n4Button);
		buttonP4.setSize(90, 30);
		n4Button.setSize(90, 30);
		buttonP4.setBackground(Color.gray);
		
		JPanel buttonP5 = new JPanel();
		JButton n5Button = new JButton("5");
		buttonP5.add(n5Button);
		buttonP5.setSize(90, 30);
		n5Button.setSize(90, 30);
		buttonP5.setBackground(Color.gray);
		
		JPanel buttonP6 = new JPanel();
		JButton n6Button = new JButton("6");
		buttonP6.add(n6Button);
		buttonP6.setSize(90, 30);
		n6Button.setSize(90, 30);
		buttonP6.setBackground(Color.gray);
		
		JPanel buttonP7 = new JPanel();
		JButton n7Button = new JButton("7");
		buttonP7.add(n7Button);
		buttonP7.setSize(90, 30);
		n7Button.setSize(90, 30);
		buttonP7.setBackground(Color.gray);
		
		JPanel buttonP8 = new JPanel();
		JButton n8Button = new JButton("8");
		buttonP8.add(n8Button);
		buttonP8.setSize(90, 30);
		n8Button.setSize(90, 30);
		buttonP8.setBackground(Color.gray);
		
		JPanel buttonP9 = new JPanel();
		JButton n9Button = new JButton("9");
		buttonP9.add(n9Button);
		buttonP9.setSize(90, 30);
		n9Button.setSize(90, 30);
		buttonP9.setBackground(Color.gray);
		
		JPanel buttonP10 = new JPanel();
		JButton n10Button = new JButton("0");
		buttonP10.add(n10Button);
		buttonP10.setSize(90, 30);
		n10Button.setSize(90, 30);
		buttonP10.setBackground(Color.gray);
		
		JPanel buttonP11 = new JPanel();
		JButton n11Button = new JButton("+");
		buttonP11.add(n11Button);
		buttonP11.setSize(90, 30);
		n11Button.setSize(90, 30);
		buttonP11.setBackground(Color.pink);
		
		JPanel buttonP12 = new JPanel();
		JButton n12Button = new JButton("-");
		buttonP12.add(n12Button);
		buttonP12.setSize(90, 30);
		n12Button.setSize(90, 30);
		buttonP12.setBackground(Color.pink);
		
		JPanel buttonP13 = new JPanel();
		JButton n13Button = new JButton("*");
		buttonP13.add(n13Button);
		buttonP13.setSize(90, 30);
		n13Button.setSize(90, 30);
		buttonP13.setBackground(Color.pink);
		
		JPanel buttonP14 = new JPanel();
		JButton n14Button = new JButton("/");
		buttonP14.add(n14Button);
		buttonP14.setSize(90, 30);
		n14Button.setSize(90, 30);
		buttonP14.setBackground(Color.pink);
		
		JPanel buttonP15 = new JPanel();
		JButton n15Button = new JButton("%");
		buttonP15.add(n15Button);
		buttonP15.setSize(90, 30);
		n15Button.setSize(90, 30);
		buttonP15.setBackground(Color.pink);
		
		JPanel buttonP16 = new JPanel();
		JButton n16Button = new JButton("=");
		buttonP16.add(n16Button);
		buttonP16.setSize(90, 30);
		n16Button.setSize(90, 30);
		buttonP16.setBackground(Color.pink);
		//to add button was created to buttonPane
		buttonPane.add(buttonP1);
		buttonPane.add(buttonP2);
		buttonPane.add(buttonP3);
		buttonPane.add(buttonP4);
		buttonPane.add(buttonP5);
		buttonPane.add(buttonP6);
		buttonPane.add(buttonP7);
		buttonPane.add(buttonP8);
		buttonPane.add(buttonP9);
		buttonPane.add(buttonP10);
		buttonPane.add(buttonP11);
		buttonPane.add(buttonP12);
		buttonPane.add(buttonP13);
		buttonPane.add(buttonP14);
		buttonPane.add(buttonP15);
		buttonPane.add(buttonP16);
		//to set the frame
		frame.setBackground(Color.gray);
		//to add Panel to the frame
		frame.add(content, BorderLayout.NORTH);
		frame.add(buttonPane, BorderLayout.AFTER_LAST_LINE);
		frame.setLocationRelativeTo(null);
		frame.pack();
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

	public static void main(String[] args) {
		//to run method was created
		SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			}
		});
	}
}