package puttarathamrongkul.panupong.lab6;

/**
 * Create by Panupong Puttarathamrongkul
 * 5530405918 lab6
 * 1. To learn how to create a window
 * 2. To learn how to use layout managers
 * 3. To learn how to create components
 * 4. To learn how to place components in the content pane
 */

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class BankAccount {
	// to exit when received action.
	private static class ButtonHandler implements ActionListener {
		public void actionPerformed(ActionEvent e) {
			System.exit(0);
		}
	}

	public static void addComponents(Container contentPane) {
		// to set contentPane to grid layout 3 lines, 2 column
		contentPane.setLayout(new GridLayout(3, 2));
		// to create label and text field
		JLabel prebalance = new JLabel(" Previous Balance: ");
		JTextField prebalanceInput = new JTextField("1000", 15);
		// to add label and text field to contentPane
		contentPane.add(prebalance);
		contentPane.add(prebalanceInput);
		JLabel amount = new JLabel(" Amount: ");
		JTextField amountInput = new JTextField(15);
		contentPane.add(amount);
		contentPane.add(amountInput);
		JLabel curbalance = new JLabel(" Curren Balance: ");
		JTextField curbalanceInput = new JTextField(15);
		contentPane.add(curbalance);
		contentPane.add(curbalanceInput);
	}

	public static void main(String[] args) {
		// to create JFrame and JPanel.
		JFrame frame = new JFrame("Simple Bank Account");
		JPanel content = new JPanel();
		// to use addComponent and receive content from JPanel.
		addComponents(content);
		// to create button.
		JPanel buttonPane = new JPanel();
		buttonPane.add(new JButton("Withdraw"));
		buttonPane.add(new JButton("Deposit"));
		JButton exitButton = new JButton("Exit");
		// to use action listener
		buttonPane.add(exitButton);
		ButtonHandler listener = new ButtonHandler();
		exitButton.addActionListener(listener);
		// to set frame's layout to border layout.
		frame.setLayout(new BorderLayout());
		// to add content and button.
		frame.add(content, BorderLayout.NORTH);
		frame.add(buttonPane, BorderLayout.AFTER_LAST_LINE);
		// to set frame as fit box and set position to center of screen.
		frame.pack();
		frame.setLocationRelativeTo(null);
		// to set frame to visible.
		frame.setVisible(true);
	}
}